import { Schema, MapSchema, type } from "@colyseus/schema";

export interface IOlivian {
    name?: string;
    color?: string;
    accent?: string;
    x?: number;
    y?: number;
    vx?: number;
    vy?: number;
    message?: string;
    messageLife?: number;
}

export class Olivian extends Schema {
    @type('string')
    name: string = '';

    @type('string')
    color: string = '';

    @type('string')
    accent: string = '';

    @type('number')
    x: number = 0;

    @type('number')
    y: number = 0;

    vx: number = 0;

    vy: number = 0;

    @type('string')
    message: string | null;

    @type('number')
    messageLife: number;

    updateWith = (obj: IOlivian) => {
        this.name = obj.name || this.name;
        this.color = obj.color || this.color;
        this.accent = obj.accent || this.accent;
        this.x = obj.x || this.x;
        this.y = obj.y || this.y;
        this.message = obj.message || this.message;
        this.messageLife = obj.messageLife || this.messageLife;
    }
}

export class MyRoomState extends Schema {

    @type({ map: Olivian })
    olivians = new MapSchema<Olivian>();

}