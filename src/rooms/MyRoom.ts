import { v4 as uuid } from 'uuid';
import { Room, Client } from "colyseus";
import { IOlivian, MyRoomState, Olivian } from "./schema/MyRoomState";

const MESSAGE_LENGTH = 5000;
const TICK_TIME = 30;

interface ClientMoveMessage {
  id: string;
  vx: number;
  vy: number;
}

interface ClientChatMessage {
  id: string;
  message: string;
}

export class MyRoom extends Room<MyRoomState> {

  interval: NodeJS.Timeout;

  tick = () => {
    this.state.olivians.forEach((olivian, id) => {
      olivian.x += olivian.vx || 0;
      olivian.y += olivian.vy || 0;
      if (olivian.messageLife > 0) {
        olivian.messageLife = Math.max(0, olivian.messageLife - TICK_TIME);
        if (olivian.messageLife === 0) olivian.message = null;
      }
      this.state.olivians.set(id, olivian);
    });
    clearInterval(this.interval);
    this.interval = setInterval(this.tick, TICK_TIME);
  }

  me = (client, { id, me }: { id: string, me: IOlivian }) => {
    const you = this.state.olivians.get(id);
    you.updateWith(me);
  }

  moveMeBro = (client, message: ClientMoveMessage) => {
    const { id, vx, vy } = message;
    const you = this.state.olivians.get(id);
    you.vx = vx;
    you.vy = vy;
  }

  chat = (client, payload: ClientChatMessage) => {
    const { id, message } = payload;
    console.log('chat', message, id);
    const you = this.state.olivians.get(id);
    you.message = message;
    you.messageLife = MESSAGE_LENGTH;
    this.state.olivians.set(id, you);
  }

  onCreate(options: any) {
    this.setState(new MyRoomState());
    this.interval = setInterval(this.tick, TICK_TIME);

    this.onMessage('moveMeBro', this.moveMeBro);
    this.onMessage('chat', this.chat);
    this.onMessage('me', this.me);
  }

  onJoin(client: Client, options: any) {
    console.log('A new client has joined!', client.id);
    const you = new Olivian();
    // const id = uuid();
    const id = client.id;
    this.state.olivians.set(id, you);
    client.send('yourId', id);
  }

  onLeave(client: Client, consented: boolean) {
    console.log('See ya, sucker', client.id);
    this.state.olivians.delete(client.id);
  }

  onDispose() {
  }

}
