let change;
let maybeSend;

window.onload = () => {
  const canvas = makeCanvas(document.body);

  const DEFAULT_USER = {
    name: 'Frog',
    color: '#b388ff',
    accent: '#ffffff',
    x: 0,
    y: 0
  };

  function isFarAway(x1, y1, x2, y2) {
    return (Math.abs(x1 - x2) > canvas.width * 0.6)
      && (Math.abs(y1 - y2) > canvas.height * 0.6);
  }

  function draw() {
    const context = canvas.getContext('2d');
    context.clearRect(0, 0, canvas.width, canvas.height);

    context.save();
    const me = state.olivians[id] || DEFAULT_USER;
    context.translate(canvas.width / 2, canvas.height / 2);

    context.translate(-me.x, -me.y);

    // draw objectos

    context.beginPath();
    context.fillStyle = '#52e4bd';
    context.fillRect(100, 100, 100, 50);
    context.closePath();

    context.beginPath();
    context.fillStyle = '#311b92';
    context.font = '20pt Arial Black';
    context.textBaseline = 'middle';
    context.textAlign = 'center';
    context.fillText('Grove', 150, 130);
    context.font = '10pt Arial';
    context.textBaseline = 'top';
    context.textAlign = 'left';
    context.fillText('the', 110, 104);
    context.closePath();

    
    // draw all players
    if (state.olivians.entries) {
      const olivians = Array.from(state.olivians.entries());

      olivians.forEach(([, olivian]) => {
        if (!olivian) return;
        const { accent, color, x, y } = olivian;
        if (isFarAway(me.x, me.y, x, y)) return;

        context.beginPath();
        context.fillStyle = color;
        context.strokeStyle = accent;
        context.lineWidth = 3;
        context.arc(x, y, 18, 0, Math.PI * 2);
        context.stroke();
        context.fill();
        context.closePath();
      });

      olivians.forEach(([, olivian]) => {
        if (!olivian) return;
        const { name, x, y } = olivian;
        if (isFarAway(me.x, me.y, x, y)) return;

        context.beginPath();
        context.fillStyle = '#ffffff';
        context.strokeStyle = '#000000';
        context.lineWidth = 3;
        context.font = '14pt Arial';
        context.textBaseline = 'bottom';
        context.textAlign = 'center';
        context.strokeText(name, x, y - 23);
        context.fillText(name, x, y - 23);
        context.closePath();
      });

      olivians.forEach(([, olivian]) => {
        if (!olivian) return;
        const { message, messageLife, x, y } = olivian;
        if (isFarAway(me.x, me.y, x, y) || !messageLife) return;

        context.save();
        context.globalAlpha = 0.9 * Math.min(1, messageLife / 100);
        context.font = '12pt Arial';
        context.textBaseline = 'top';
        context.textAlign = 'left';
        const { width } = context.measureText(message);
        context.fillStyle = '#ffffff';
        context.fillRect(x - 8, y - 80, width + 16, 34);
        context.beginPath();
        context.moveTo(x - 4, y - 46);
        context.lineTo(x - 4, y - 35);
        context.lineTo(x + 10, y - 46);
        context.lineTo(x - 4, y - 46);
        context.fill();
        context.closePath();
        context.fillStyle = '#000000';
        context.fillText(message, x, y - 68);
        context.restore();
      });
    }

    context.restore();

    window.requestAnimationFrame(draw);
  }

  window.requestAnimationFrame(draw);
};

const MODES = {
  WALKING: 'walking',
  TALKING: 'talking',
};

let id = '0';
const localState = {
  id,
  mode: MODES.WALKING,
}

let state = {
  olivians: {},
};

const KEY = {
  DOWN: 83, // S
  LEFT: 65, // A
  RIGHT: 68, // D 
  UP: 87, // W
  CHAT: 69, // E
  ENTER: 13, // ENTER
  ESCAPE: 27, // ESCAPE
}

const WALKING_KEYS = [KEY.DOWN, KEY.UP, KEY.LEFT, KEY.RIGHT];
const SPEED = 7;
const pressed = {};

function handleKeydown(room, keyCode) {
  if (pressed[keyCode]) return;
  pressed[keyCode] = true;
  handleKeys(room, keyCode);
}

function handleKeyup(room, keyCode) {
  pressed[keyCode] = false;
  handleKeys(room, keyCode, true);
}

function handleKeys(room, keyCode, keyup) {
  if (keyup && MODES.WALKING && keyCode === KEY.CHAT) {
    return handleChat();
  }
  if (MODES.WALKING && WALKING_KEYS.includes(keyCode)) {
    return handleMovement(room);
  }
}

function handleChat() {
  localState.mode = MODES.TALKING;
  document.getElementById('chat').classList.add('show');
  document.querySelector('#chat > input').focus();
}

function handleMovement(room) {
  let x = 0,
    y = 0;

  if (pressed[KEY.UP]) y = -1;
  else if (pressed[KEY.DOWN]) y = 1;

  if (pressed[KEY.LEFT]) x = -1;
  else if (pressed[KEY.RIGHT]) x = 1;

  // calculate movement
  const angle = Math.atan2(y, x);
  let vx = SPEED * Math.cos(angle);
  let vy = SPEED * Math.sin(angle);

  if (x === 0 || Math.abs(vx) < 0.01) vx = 0;
  if (y === 0 || Math.abs(vy) < 0.01) vy = 0;

  room.send('moveMeBro', {
    id,
    vx,
    vy,
  });
}

function connect() {
  var host = window.document.location.host.replace(/:.*/, '');

  // const server = 'ws://olive-grove.herokuapp.com/';
  // const server = 'ws://localhost:2567';
  const server = location.protocol.replace("http", "ws") + "//" + host + (location.port ? ':' + location.port : '');
  var client = new Colyseus.Client(server);

  client.joinOrCreate('chat').then(room => {
    console.log('joined');

    room.onMessage('yourId', myId => {
      id = myId;
      const saved = JSON.parse(window.localStorage.getItem('settings')) || {};
      const me = {
        name: saved.name || 'Frog',
        color: saved.color || '#b388ff',
        accent: saved.accent || '#ffffff',
      };
      room.send('me', { id, me });
    });

    room.onStateChange.once(function (gameState) {
      state = gameState;
    });

    // new room state
    room.onStateChange(function (gameState) {
      state = gameState;
    });

    change = (property, { target }) => {
      const saved = JSON.parse(window.localStorage.getItem('settings')) || {};
      saved[property] = target.value;
      window.localStorage.setItem('settings', JSON.stringify(saved));
      
      room.send('me', { id, me: saved });
    }

    maybeSend = ({ target, keyCode }) => {
      if ([KEY.ENTER, KEY.ESCAPE].includes(keyCode)) {
        const message = target.value;
        // reset chat window
        target.value = '';
        document.getElementById('chat').classList.remove('show');
        localState.mode = MODES.WALKING;
        target.blur();

        if (keyCode === KEY.ENTER && message) room.send('chat', { id, message });
      }
    }

    window.addEventListener('keydown', ({
      keyCode
    }) => {
      handleKeydown(room, keyCode);
    });

    window.addEventListener('keyup', ({
      keyCode
    }) => {
      handleKeyup(room, keyCode);
    });

  }).catch(e => {
    console.log('JOIN ERROR', e);
  });
}

connect();