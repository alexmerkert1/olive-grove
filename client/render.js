function makeCanvas (root, options = {}) {
  const { width = 1600, height = 900 } = options;
  const canvas = document.createElement('canvas');
  canvas.width = width;
  canvas.height = height;
  root.appendChild(canvas);

  const resizeCanvas = function () {
    const normalRatio = canvas.width / canvas.height;
    const newRatio = root.offsetWidth / root.offsetHeight;
    let scale = 1;
    if (newRatio < normalRatio) {
      // tall and skinny
      scale = root.offsetWidth / canvas.width;
    } else if (newRatio >= normalRatio) {
      // short and fat
      scale = root.offsetHeight / canvas.height;
    }
    canvas.style.transform = 'translate(-50%, -50%) scale(' + scale + ', ' + scale + ')';
  }

  window.addEventListener('resize', event => {
    resizeCanvas();
  });

  setTimeout(resizeCanvas, 10);

  return canvas;
}